//
//  CriticsViewController.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 10.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import UIKit
import Moya
import Moya_ObjectMapper

class CriticsViewController: BaseViewController {
    
    private static let lineSpacing: CGFloat = 8.0
    private static let cellHeightMultiplier: CGFloat = 160 / 176 // size for cell in iphone 7 (height / width)
    private static let headerViewHeight: CGFloat = 88.0
    
    private static let cellId = String(describing: CriticCollectionViewCell.self)
    private static let headerCellId = String(describing: CriticHeaderCollectionView.self)
    
    // MARK: - Base overrides
    
    override func initDownloadManager() {
        dataDownloadManager = CriticDownloadManager()
    }
    
    override func reloadData() {
        (contentView as! UICollectionView).reloadData()
    }
    
    override func setupContentView() {
        super.setupContentView()
        let collectionView = (contentView as! UICollectionView)
        collectionView.registerCellId(CriticsViewController.cellId)
        collectionView.registerHeaderCellId(CriticsViewController.headerCellId)
        setupFlowLayout()
    }
    
    // MARK: - Stylization
    
    private func setupFlowLayout() {
        let layout = UICollectionViewFlowLayout()
        let space = CriticsViewController.lineSpacing
        layout.minimumLineSpacing = space
        layout.sectionInset = UIEdgeInsetsMake(space, space, space, space)
        (contentView as! UICollectionView).setCollectionViewLayout(layout, animated: false)
    }
    
}

// MARK: - UICollectionViewDelegate, UICollectionViewDataSource
extension CriticsViewController: UICollectionViewDelegate, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return (dataDownloadManager as! CriticDownloadManager).critics.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CriticsViewController.cellId, for: indexPath) as! CriticCollectionViewCell
        let critic = (dataDownloadManager as! CriticDownloadManager).critics[indexPath.row]
        cell.displayCellContent(critic: critic)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        switch kind {
        case UICollectionElementKindSectionHeader:
            let view = collectionView.dequeueReusableSupplementaryView(ofKind: UICollectionElementKindSectionHeader, withReuseIdentifier: CriticsViewController.headerCellId, for: indexPath) as! CriticHeaderCollectionView
            view.delegate = self
            return view
        default:
            return UICollectionReusableView()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let critic = (dataDownloadManager as! CriticDownloadManager).critics[indexPath.row]
        navigationController?.pushViewController(CriticInfoViewController(critic: critic), animated: true)
    }
    
}

// MARK: UICollectionViewDelegateFlowLayout
extension CriticsViewController: UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = (Constants.screenWidth - CriticsViewController.lineSpacing * 4) / 2.0
        return CGSize(width: width, height: width * CriticsViewController.cellHeightMultiplier)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize(width: Constants.screenWidth, height: CriticsViewController.headerViewHeight)
    }
    
}
