//
//  BaseViewController.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 12.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import UIKit

protocol HeaderViewDelegate: class {
    func headerViewStopEditing(searchString: String, dateString: String?)
}

class BaseViewController: UIViewController {
    
    @IBOutlet weak var contentView: UIScrollView!
    
    private var searchString: String = ""
    private var dateString: String = ""
    var reviewerString: String = ""
    
    var dataDownloadManager: DataDownloadManager!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        initDownloadManager()
        setupContentView()
        startLoading()
    }
    
    // MARK: - Stylization
    
    func setupContentView() {
        addInfiniteScroll()
    }
    
    private func addInfiniteScroll() {
        contentView.addInfiniteScroll { [weak self] (tableView) in
            guard let weakSelf = self else { return }
            weakSelf.infiniteScrollTriggered()
        }
        contentView.setShouldShowInfiniteScrollHandler { [weak self] (tableView) -> Bool in
            guard let weakSelf = self else { return false }
            return weakSelf.dataDownloadManager.hasNextPage()
        }
        contentView.createAndAddRefreshControlWith(self, andSelector: #selector(refreshControlTriggered))
    }
    
    // MARK: - Handlers
    
    @objc private func refreshControlTriggered() {
        contentView.finishInfiniteScroll()
        dataDownloadManager.loadFirstPage(searchString: searchString, publicationDate: dateString, reviewer: reviewerString, withCompletion: getCompletionLoadingHandler())
    }
    
    private func infiniteScrollTriggered() {
        contentView.endRefreshing()
        dataDownloadManager.loadNextPage(searchString: searchString, publicationDate: dateString, reviewer: reviewerString, withCompletion: getCompletionLoadingHandler())
    }
    
    // MARK: - Helpers
    
    func initDownloadManager() {
        preconditionFailure("Must be overriden in subclasses")
    }
    
    func reloadData() {
        preconditionFailure("Must be overriden in subclasses")
    }
    
    private func startLoading() {
        contentView.pullToRefresh()
        refreshControlTriggered()
    }
    
    private func getCompletionLoadingHandler() -> CommonErrorCompletion {
        return { [weak self] (error) in
            guard let weakSelf = self else { return }
            weakSelf.finishLoadingMediasets(withError: error)
        }
    }
    
    private func finishLoadingMediasets(withError error: Error?) {
        contentView.finishInfiniteScroll()
        contentView.endRefreshing()
        if let _ = error {
            //showError(err)
        } else {
            reloadData()
        }
    }
}

// MARK: - HeaderViewDelegate
extension BaseViewController: HeaderViewDelegate {
    func headerViewStopEditing(searchString: String, dateString: String?) {
        if (self.searchString != searchString) || (self.dateString != dateString) {
            self.searchString = searchString
            self.dateString = dateString ?? ""
            startLoading()
        }
    }
}
