//
//  ContainerViewController.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 09.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import UIKit

class ContainerViewController: UIViewController {

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var segmentControl: UISegmentedControl!
    
    private var currentViewController: UIViewController?
    private var _childViewControllers = [ReviewsViewController(), CriticsViewController()]
    
    // MARK: - View Controller Lifecycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = String.reviewsTitle
        hideNavigationBarBottomLine()
        setupSegmentControl()
        displayCurrentTab(0)
    }
    
    private func setupSegmentControl() {
        segmentControl.setTitle(String.reviewsTitle, forSegmentAt: 0)
        segmentControl.setTitle(String.criticsTitle, forSegmentAt: 1)
    }
    
    // MARK: - Switching Tabs Functions
   
    @IBAction func switchTabs(_ sender: UISegmentedControl) {
        currentViewController?.removeViewController()
        title = sender.titleForSegment(at: sender.selectedSegmentIndex)
        displayCurrentTab(sender.selectedSegmentIndex)
    }
    
    func viewControllerForSelectedSegmentIndex(_ index: Int) -> UIViewController? {
        if _childViewControllers.indices.contains(index) {
            return _childViewControllers[index]
        }
        return nil
    }
    
    func displayCurrentTab(_ tabIndex: Int){
        if let viewController = viewControllerForSelectedSegmentIndex(tabIndex) {
            addViewController(viewController, containerView: contentView)
            currentViewController = viewController
        }
    }
}
