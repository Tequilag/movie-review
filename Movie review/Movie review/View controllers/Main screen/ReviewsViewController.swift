//
//  ReviewsViewController.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 09.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import UIKit
import UIScrollView_InfiniteScroll

class ReviewsViewController: BaseViewController {
    
    private static let headerHeight: CGFloat = 149.0
    private static let cellHeightMultiplier = 173.0 / Constants.screenHeight
    
    private static let cellId = String(describing: ReviewTableViewCell.self)
    var headerView: UIView!
    
    override func viewDidLoad() {
        initHeaderView()
        super.viewDidLoad()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        sizeHeaderFit()
    }
    
    // MARK: - Base overrides
    
    override func initDownloadManager() {
        dataDownloadManager = ReviewDownloadManager()
    }
    
    override func reloadData() {
        (contentView as! UITableView).reloadData()
    }
    
    // MARK: - Stylization
    
    override func setupContentView() {
        super.setupContentView()
        let tableView = (contentView as! UITableView)
        let cellHeight = Constants.screenHeight * ReviewsViewController.cellHeightMultiplier
        tableView.rowHeight = cellHeight
        tableView.estimatedRowHeight = cellHeight
        tableView.registerCellId(ReviewsViewController.cellId)
        tableView.tableHeaderView = headerView
    }
    
    func initHeaderView() {
        headerView = ReviewTableHeaderView.loadView() as! ReviewTableHeaderView
        (headerView as! ReviewTableHeaderView).delegate = self
    }
    
    func sizeHeaderFit() {
        headerView.frame.size.height = ReviewsViewController.headerHeight
    }
}

// MARK: - UITableViewDelegate, UITableViewDataSource
extension ReviewsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (dataDownloadManager as! ReviewDownloadManager).reviews.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: ReviewsViewController.cellId, for: indexPath) as! ReviewTableViewCell
        let review = (dataDownloadManager as! ReviewDownloadManager).reviews[indexPath.row]
        cell.displayCellContent(review: review)
        return cell
    }

}
