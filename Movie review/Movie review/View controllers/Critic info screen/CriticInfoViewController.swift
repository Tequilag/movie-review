//
//  CriticInfoViewController.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 13.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import UIKit

class CriticInfoViewController: ReviewsViewController {
    
    var critic: CriticEntity! = nil
    
    convenience init(critic: CriticEntity) {
        self.init()
        self.critic = critic
    }
    
    // MARK: - Base overrides

    override func initHeaderView() {
        headerView = CriticInfoHeaderView.loadView()
        (headerView as! CriticInfoHeaderView).displayCriticInfo(critic: critic)
        (headerView as! CriticInfoHeaderView).delegate = self
        reviewerString = critic.displayName
    }

    override func sizeHeaderFit() {
        let tableView = contentView as! UITableView
        
        if let headerView = tableView.tableHeaderView {
            headerView.setNeedsLayout()
            headerView.layoutIfNeeded()
            
            let height = headerView.systemLayoutSizeFitting(UILayoutFittingCompressedSize).height
            var frame = headerView.frame
            frame.size.height = height
            
            headerView.frame = frame
            tableView.tableHeaderView = headerView
            
        }
    }

}

// MARK: - CriticInfoHeaderViewDelegate
extension CriticInfoViewController: CriticInfoHeaderViewDelegate {
    func detailInfoButtonTapped() {
        self.sizeHeaderFit()
    }
}
