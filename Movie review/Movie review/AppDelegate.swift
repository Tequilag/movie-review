//
//  AppDelegate.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 09.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        setupKeyboardManager()
        setupNavigationBarAppereance()
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = UINavigationController(rootViewController: ContainerViewController())
        window?.makeKeyAndVisible()
        return true
    }
    
    private func setupNavigationBarAppereance() {
        func setupNavigationBarAppereance(){
            let navigationBarAppearace = UINavigationBar.appearance()
            navigationBarAppearace.barStyle = .black
            navigationBarAppearace.isTranslucent = false
            navigationBarAppearace.tintColor =  .white
            navigationBarAppearace.barTintColor = .navBarBackgroundColor

        }
    }
    
    private func setupKeyboardManager() {
        let keyBoardManager = IQKeyboardManager.sharedManager()
        keyBoardManager.enable = true
        keyBoardManager.enableAutoToolbar = false
        keyBoardManager.shouldResignOnTouchOutside = true
        keyBoardManager.keyboardAppearance = .dark
        keyBoardManager.overrideKeyboardAppearance = true
    }

}

