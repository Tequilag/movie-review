//
//  Constants.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 10.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import UIKit

typealias CommonErrorCompletion = (Error?) -> Void

struct Constants {
    
    static let movieReviewsAPIKey = "7d8aec4b03404ae7a3b66f3470b07fd8"
    
    static let screenHeight = max(UIScreen.main.bounds.height, UIScreen.main.bounds.width)
    static let screenWidth = min(UIScreen.main.bounds.height, UIScreen.main.bounds.width)
    
}
