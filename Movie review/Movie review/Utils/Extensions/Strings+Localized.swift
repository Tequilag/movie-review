//
//  Strings+Localized.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 15.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import UIKit

fileprivate func NSLocalizedString(_ key: String) -> String {
    return NSLocalizedString(key, comment: "")
}

extension String {
    static var reviewsTitle: String { return NSLocalizedString("reviewsTitle") }
    static var criticsTitle: String { return NSLocalizedString("criticsTitle") }
    static var biographyButtonTitle: String { return NSLocalizedString("biographyButtonTitle") }
    static var statusString: String { return NSLocalizedString("statusString") }
}
