//
//  Colors.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 11.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import UIKit

extension UIColor {
    static let navBarBackgroundColor = r(181, g: 226, b: 250)
}

extension UIColor {
    static func r(_ r: CGFloat, g: CGFloat, b: CGFloat) -> UIColor {
        return UIColor(red: r / 255.0, green: g / 255.0, blue: b / 255.0, alpha: 1)
    }
}
