//
//  UITextField+setTextFieldImage.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 13.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import UIKit

extension UITextField {

    func withImage(image: UIImage, colorBorder: UIColor) {
        
        let imageSize = frame.size.height / 2.0
        let offset: CGFloat = 10.0
        
        // imageSize+offset is to make space beetween text and image
        let mainView = UIView(frame: CGRect(x: 0, y: 0, width: imageSize + offset, height: imageSize))

        let imageView = UIImageView(image: image)
        imageView.contentMode = .scaleToFill
        imageView.frame = CGRect(x: offset, y: 0, width: imageSize, height: imageSize)
        mainView.addSubview(imageView)
        
        leftViewMode = .always
        leftView = mainView

    }
}
