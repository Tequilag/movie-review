//
//  UIViewController+Container.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 09.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import UIKit

extension UIViewController {
    func addViewController(_ viewController: UIViewController, containerView: UIView) {
        if viewController.parent == nil {
            viewController.willMove(toParentViewController: self)
            addChildViewController(viewController)
            containerView.addFullSizeView(view: viewController.view)
            viewController.didMove(toParentViewController: self)
        }
    }
    
    func removeViewController() {
        if parent != nil {
            willMove(toParentViewController: nil)
            view.removeFromSuperview()
            removeFromParentViewController()
        }
    }
}
