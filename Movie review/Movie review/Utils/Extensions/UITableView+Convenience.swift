//
//  UITableView+Convenience.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 12.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import UIKit

extension UITableView {
    func registerCellId(_ cellId: String) {
        register(UINib(nibName: cellId, bundle: nil), forCellReuseIdentifier: cellId)
    }
}
