//
//  UIViewController+showError.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 12.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import UIKit

extension UIViewController {
    func showError(_ error: Error) {
        let alertController = UIAlertController(title: "Error", message: error.localizedDescription, preferredStyle: .alert)
        alertController.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        present(alertController, animated: true, completion: nil)
    }
}
