//
//  UIViewController+HideNavigationBarBottomLine.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 11.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import UIKit

extension UIViewController {
    func hideNavigationBarBottomLine() {
        let nav = navigationController?.navigationBar
        nav?.isTranslucent = false
        let image = UIImage()
        nav?.shadowImage = image
        nav?.setBackgroundImage(image, for: .default)
        nav?.barTintColor = UIColor.navBarBackgroundColor
    }
}
