//
//  UICollectionView+Convenience.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 13.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import UIKit

extension UICollectionView {
    func registerCellId(_ cellId: String) {
        register(UINib(nibName: cellId, bundle: nil), forCellWithReuseIdentifier: cellId)
    }
    
    func registerHeaderCellId(_ headerCellId: String) {
        register(UINib(nibName: headerCellId, bundle: nil), forSupplementaryViewOfKind: UICollectionElementKindSectionHeader, withReuseIdentifier: headerCellId)
    }
}

