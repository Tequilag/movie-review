//
//  MoyaResponse+mapError.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 12.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import Moya

extension Response {
    func mapError(domain: AnyClass) -> Error {
        do {
            let errorResponse = try mapObject(ErrorResponse.self)
            let error = NSError(domain: String(describing: domain), code: -1, userInfo: [NSLocalizedDescriptionKey : errorResponse.message])
            return error
        }
        catch (let error) {
            return error
        }
    }
}
