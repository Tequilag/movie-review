//
//  NSError+isCanceled.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 12.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import UIKit

extension Error {
    func isCanceled() -> Bool {
        let nsError = self as NSError
        return nsError.code == NSURLErrorCancelled
    }
}

