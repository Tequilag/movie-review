//
//  UIImageView+SDActivityIndicator.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 11.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import UIKit

extension UIImageView {
    func setupImageViewActivityIndicator() {
        sd_setIndicatorStyle(.gray)
        sd_setShowActivityIndicatorView(true)
    }
}
