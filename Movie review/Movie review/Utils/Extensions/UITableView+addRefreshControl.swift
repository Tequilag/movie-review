//
//  UITableView+addRefreshControl.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 12.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//


import UIKit

extension UIScrollView {
    func createAndAddRefreshControlWith(_ target: Any?, andSelector selector: Selector) {
        let refreshControl = UIRefreshControl()
        refreshControl.tintColor = .black
        refreshControl.addTarget(target, action: selector, for: .valueChanged)
        refreshControl.layer.zPosition = -1
        addSubview(refreshControl)
    }
    
    func pullToRefresh() {
        if let refreshControl = getRefreshControll() {
            setContentOffset(CGPoint(x: 0, y: -refreshControl.frame.size.height), animated: true)
            refreshControl.beginRefreshing()
        }
    }
    
    func startRefreshing() {
        if let refreshControl = getRefreshControll() {
            refreshControl.beginRefreshing()
        }
    }
    
    func endRefreshing() {
        if let refreshControl = getRefreshControll() {
            refreshControl.endRefreshing()
        }
    }
    
    func removeRefreshControl() {
        if let refreshControl = getRefreshControll() {
            refreshControl.removeFromSuperview()
        }
    }
    
    func getRefreshControll() -> UIRefreshControl? {
        var refreshControl: UIRefreshControl?
        for subview in subviews {
            if subview is UIRefreshControl {
                refreshControl = subview as? UIRefreshControl
                break
            }
        }
        return refreshControl
    }
}

