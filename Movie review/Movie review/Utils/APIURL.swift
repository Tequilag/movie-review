//
//  APIURL.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 10.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

struct APIURL {
    
    static let baseURL = "https://api.nytimes.com/svc/movies/v2"
    static let criticList = "/critics/all.json"
    static let reviewList = "/reviews/all.json"
    static let searchReviews = "/reviews/search.json"
    
    static func getCritic(_ byName: String) -> String {
        return String(format: "/critics/\(byName).json")
    }
}

