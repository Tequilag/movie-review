//
//  LinkEntity.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 10.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import ObjectMapper

class LinkEntity: Mappable {
    var type: String!
    var url: String!
    var suggestedLinkText: String!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        type <- map["type"]
        url <- map["url"]
        suggestedLinkText <- map["suggested_link_text"]
    }
}
