//
//  ReviewListEntity.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 10.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import ObjectMapper

class ReviewListEntity: BaseListEntity {
    
    var results: [ReviewEntity] = []
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        hasMore <- map["has_more"]
        results <- map["results"]
    }
}
