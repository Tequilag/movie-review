//
//  ReviewEntity.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 10.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import ObjectMapper

class ReviewEntity: Mappable {
    var displayTitle: String!
    var mpaaRating: String!
    var criticsPick: String!
    var byline: String!
    var headline: String!
    var summaryShort: String!
    var publicationDate: String!
    var openingDate: String!
    var dateUpdated: String!
    var link: LinkEntity!
    var multimedia: ResourceEntity?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        displayTitle <- map["display_title"]
        mpaaRating <- map["mpaa_rating"]
        criticsPick <- map["critics_pick"]
        byline <- map["byline"]
        headline <- map["headline"]
        summaryShort <- map["summary_short"]
        publicationDate <- map["publication_date"]
        openingDate <- map["opening_date"]
        dateUpdated <- map["date_updated"]
        link <- map["link"]
        multimedia <- map["multimedia"]
    }
}
