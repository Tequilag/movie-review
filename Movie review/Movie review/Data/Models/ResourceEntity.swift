//
//  ResourceEntity.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 10.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import ObjectMapper

class ResourceEntity: Mappable {
    var type: String!
    var source: String!
    var height: Int!
    var width: Int!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        type <- map["type"]
        source <- map["src"]
        height <- map["height"]
        width <- map["width"]
    }
}

