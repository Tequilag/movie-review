//
//  ErrorResponse.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 12.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import ObjectMapper

class ErrorResponse: Mappable {
    var message: String!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        message <- map["error"]
    }
}
