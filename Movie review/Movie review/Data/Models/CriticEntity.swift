//
//  CriticEntity.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 10.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import ObjectMapper

class CriticEntity: Mappable {
    var displayName: String!
    var sortName: String!
    var status: String!
    var bio: String!
    var seoName: String!
    var multimedia: CriticResourceEntity?
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        displayName <- map["display_name"]
        sortName <- map["sort_name"]
        status <- map["status"]
        bio <- map["bio"]
        seoName <- map["seo_name"]
        multimedia <- map["multimedia.resource"]
    }
}
