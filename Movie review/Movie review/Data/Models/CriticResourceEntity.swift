//
//  CriticResourceEntity.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 10.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import ObjectMapper

class CriticResourceEntity: ResourceEntity {
    var credit: String!
    
    override func mapping(map: Map) {
        super.mapping(map: map)
        credit <- map["credit"]
    }
}


