//
//  BaseListEntity.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 11.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import ObjectMapper

class BaseListEntity: Mappable {
    var status: String!
    var copyright: String!
    var hasMore: Bool?
    var resultCount: Int!
    
    required init?(map: Map) {}
    
    func mapping(map: Map) {
        status <- map["status"]
        copyright <- map["copyright"]
        resultCount <- map["num_results"]
    }
}
