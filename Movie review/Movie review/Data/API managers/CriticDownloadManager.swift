//
//  DataDownloadManager.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 10.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import Moya

class CriticDownloadManager: DataDownloadManager {

    var critics: [CriticEntity] = []
    
    // MARK: - Base overrides
    
    override func getTarget(offset: Int, searchString: String, publicationDate: String, reviewer: String) -> MoyaProvider<APIService>.Target {
        if !searchString.isEmpty {
            return .getCritic(byName: searchString)
        }
        return .getCriticList
    }
    
    override func getResponse(response: Response, withCompletion completion: @escaping CommonErrorCompletion) -> BaseListEntity? {
        do {
            return try response.mapObject(CriticListEntity.self)
        } catch {
            completion(response.mapError(domain: DataDownloadManager.self))
            return nil
        }
    }
    
    override func handleDownloadedResponse(_ response: BaseListEntity) {
        super.handleDownloadedResponse(response)
        critics = (response as! CriticListEntity).results
    }
    
}
