//
//  DataDownloadManager.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 11.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import Moya

class DataDownloadManager {
    
    private let provider = MoyaProvider<APIService>()
    private var lastRequest: Cancellable?
    private var lastResponse: BaseListEntity?
    private(set) var offset = 0
    
    // MARK: - Base overrides
    
    func getTarget(offset: Int, searchString: String, publicationDate: String, reviewer: String) -> MoyaProvider<APIService>.Target {
        preconditionFailure("Must be overridden in subclasses")
    }
    
    func getResponse(response: Response, withCompletion completion: @escaping CommonErrorCompletion) -> BaseListEntity? {
        preconditionFailure("Must be overridden in subclasses")
    }
    
    func handleDownloadedResponse(_ response: BaseListEntity) {
        lastResponse = response
        offset += response.resultCount
    }
    
    // MARK: - Interface
    
    func hasNextPage() -> Bool {
        if let hasMore = lastResponse?.hasMore {
            return hasMore
        }
        return false
    }
    
    func loadFirstPage(searchString: String, publicationDate: String, reviewer: String, withCompletion completion: @escaping CommonErrorCompletion) {
        offset = 0
        loadNextPage(searchString: searchString, publicationDate: publicationDate, reviewer: reviewer, withCompletion: completion)
    }
    
    func loadNextPage(searchString: String, publicationDate: String, reviewer: String, withCompletion completion: @escaping CommonErrorCompletion) {
        let target = getTarget(offset: offset, searchString: searchString, publicationDate: publicationDate, reviewer: reviewer)
        loadDataFromTarget(target, withCompletion: completion)
    }
    
    // MARK: - Networking
    
    private func loadDataFromTarget(_ target: MoyaProvider<APIService>.Target, withCompletion completion: @escaping CommonErrorCompletion) {
        if let request = lastRequest, !request.isCancelled {
            request.cancel()
        }
        lastRequest = provider.request(target) { [weak self] (result) in
            guard let weakSelf = self else { return }
            switch result {
            case .success(let response):
                do {
                    let filtered = try response.filterSuccessfulStatusCodes()
                    let response = weakSelf.getResponse(response: filtered, withCompletion: completion)
                    if let _response = response {
                        weakSelf.handleDownloadedResponse(_response)
                        completion(nil)
                    }
                } catch {
                    completion(response.mapError(domain:DataDownloadManager.self))
                }
            case let .failure(error):
                switch error {
                case .underlying(let err, _):
                    if err.isCanceled() { return }
                default:
                    break
                }
                completion(error)
            }
        }
    }
    
}
