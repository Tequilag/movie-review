//
//  ReviewDownloadManager.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 11.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import Moya

class ReviewDownloadManager: DataDownloadManager {

    var reviews: [ReviewEntity] = []
    
    // MARK: - Base overrides
    
    override func getTarget(offset: Int, searchString: String, publicationDate: String, reviewer: String) -> MoyaProvider<APIService>.Target {
        if !(searchString.isEmpty) || !(publicationDate.isEmpty) || !(reviewer.isEmpty){
            return .searchReviews(date: publicationDate, keyWords: searchString, offset: offset, reviewer: reviewer)
        }
        return .getReviews(offset: offset)
    }
    
    override func getResponse(response: Response, withCompletion completion: @escaping CommonErrorCompletion) -> BaseListEntity? {
        do {
            return try response.mapObject(ReviewListEntity.self)
        } catch {
            completion(response.mapError(domain: DataDownloadManager.self))
            return nil
        }
    }
    
    override func handleDownloadedResponse(_ response: BaseListEntity) {
        if (super.offset == 0) {
            reviews = []
        }
        super.handleDownloadedResponse(response)
        reviews += (response as! ReviewListEntity).results
    }
    
}
