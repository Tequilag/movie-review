//
//  APIService.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 10.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import Moya

enum APIService {
    case getCriticList
    case getCritic(byName: String)
    case getReviews(offset: Int)
    case searchReviews(date: String, keyWords: String, offset: Int, reviewer: String)
}

extension APIService: TargetType {
    
    private static let APIKey: String = Constants.movieReviewsAPIKey
    
    var baseURL: URL { return URL(string: APIURL.baseURL)! }
    
    var path: String {
        switch self {
        case .getCriticList:
            return APIURL.criticList
        case .getCritic(let name):
            return APIURL.getCritic(name)
        case .getReviews(_):
            return APIURL.reviewList
        case .searchReviews(_, _, _, _):
            return APIURL.searchReviews
        }
    }
    
    var method: Moya.Method {
        switch self {
        case .getCritic, .getCriticList:
            return .get
            
        case .getReviews, .searchReviews:
            return .get
        }
    }
    
    var sampleData: Data {
        return Data()
    }
    
    var task: Task {
        switch self {
            
        case .getReviews(let offset):
            return .requestParameters(parameters: ["api-key": APIService.APIKey, "offset": offset], encoding: URLEncoding.queryString)
            
        case .searchReviews(let date, let keyWords, let offset, let reviewer):
            var parameters: [String : String] = ["api-key": APIService.APIKey, "offset": "\(offset)"]
            
            if !date.isEmpty {
                parameters["publication-date"] = date
            }
            
            if !keyWords.isEmpty {
                parameters["query"] = keyWords
            }
            
            if !reviewer.isEmpty {
                parameters["reviewer"] = reviewer
            }
            
            return .requestParameters(parameters: parameters, encoding: URLEncoding.queryString)
            
        default:
            return .requestParameters(parameters: ["api-key": APIService.APIKey], encoding: URLEncoding.queryString)
        }
        
    }
    
    var headers: [String : String]? {
        return ["Content-type": "application/json"]
    }
}
