//
//  CriticCollectionViewCell.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 10.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import UIKit
import SDWebImage

class CriticCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        imageView.setupImageViewActivityIndicator()
    }
    
    // MARK: - Prepare for reuse
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.sd_cancelCurrentImageLoad()
        imageView.image = UIImage(named: "defaultAvatar")
    }
    
    // MARK: - Helpers

    func displayCellContent(critic: CriticEntity) {
        nameLabel.text = critic.displayName
        if let url = critic.multimedia?.source {
            setImage(url: URL(string: url))
        }
    }
    
    private func setImage(url: URL?) {
        if let source = url {
            imageView.sd_setImage(with: source, completed: nil)
        }
    }

}
