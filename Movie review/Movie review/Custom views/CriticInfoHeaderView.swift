//
//  CriticInfoHeaderView.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 13.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import UIKit
import SDWebImage

protocol CriticInfoHeaderViewDelegate: class {
    func detailInfoButtonTapped()
}

class CriticInfoHeaderView: UIView {
    
    weak var delegate: CriticInfoHeaderViewDelegate?

    @IBOutlet weak var contentView: UIView!
    @IBOutlet weak var authorImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var biographyButton: UIButton!
    @IBOutlet weak var biographyLabel: UILabel!
    
    private var biographyText: String = ""
    
    override func awakeFromNib() {
        super.awakeFromNib()
        authorImageView.setupImageViewActivityIndicator()
        biographyButton.setTitle(String.biographyButtonTitle, for: .normal)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        biographyLabel.preferredMaxLayoutWidth = biographyLabel.bounds.width
    }
    
    // MARK: - Helpers
    
    func displayCriticInfo(critic: CriticEntity) {
        nameLabel.text = critic.displayName
        statusLabel.text = String.statusString + ": " + critic.status
        biographyText = critic.bio
        biographyButton.isEnabled = !biographyText.isEmpty
        if let source = critic.multimedia?.source {
            authorImageView.sd_setImage(with: URL(string: source), placeholderImage: UIImage(named: "defaultAvatar"), completed: nil)
        }
    }

    // NARK: - Handlers
    
    @IBAction func biographyButtonTapped(_ sender: UIButton) {
        if let text = biographyLabel.text {
            biographyLabel.text = text.isEmpty ? biographyText : ""
            delegate?.detailInfoButtonTapped()
        }
    }

}
