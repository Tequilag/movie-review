//
//  ReviewTableViewCell.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 11.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import UIKit
import SDWebImage

class ReviewTableViewCell: UITableViewCell {
    
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var previewTextLabel: UILabel!
    @IBOutlet weak var authorLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        articleImageView.setupImageViewActivityIndicator()
    }
    
    // MARK: - Prepare for reuse
    
    override func prepareForReuse() {
        super.prepareForReuse()
        articleImageView.sd_cancelCurrentImageLoad()
        articleImageView.image = nil
    }
    
    // MARK: - Helpers
    
    func displayCellContent(review: ReviewEntity) {
        titleLabel.text = review.displayTitle
        previewTextLabel.text = review.summaryShort
        authorLabel.text = review.byline
        dateLabel.text = review.dateUpdated
        if let url = review.multimedia?.source {
            setImage(url: URL(string: url))
        }
    }
    
    private func setImage(url: URL?) {
        if let source = url {
            articleImageView.sd_setImage(with: source, completed: nil)
        }
    }
    
}
