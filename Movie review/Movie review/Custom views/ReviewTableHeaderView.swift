//
//  ReviewTableHeaderView.swift
//  Movie review
//
//  Created by Gorbenko Georgy on 11.07.18.
//  Copyright © 2018 Gorbenko Georgy. All rights reserved.
//

import UIKit

class ReviewTableHeaderView: UICollectionReusableView {
    
    weak var delegate: HeaderViewDelegate?

    @IBOutlet weak var searchTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
  
    override func awakeFromNib() {
        super.awakeFromNib()
        setupTextFields()
    }
    
    // MARK: - Stylization
    
    private func setupTextFields() {
        searchTextField.withImage(image: UIImage(named: "loopIcon")!, colorBorder: .clear)
        dateTextField?.withImage(image: UIImage(named: "calendarIcon")!, colorBorder: .clear)
    }
    
    private func showDatePicker(_ sender: UITextField) {
        let datePickerView: UIDatePicker = UIDatePicker()
        datePickerView.datePickerMode = .date
        sender.inputView = datePickerView
        datePickerView.addTarget(self, action: #selector(datePickerValueChanged), for: .valueChanged)
        if !dateTextField.hasText {
            datePickerValueChanged(sender: datePickerView)
        }
    }
    
    // MARK: - Handlers
    
    @objc private func datePickerValueChanged(sender: UIDatePicker) {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "YYYY-MM-dd"
        dateTextField.text = dateFormatter.string(from: sender.date)
    }

}

// MARK: - UITextFieldDelegate
extension ReviewTableHeaderView: UITextFieldDelegate {
    func textFieldDidEndEditing(_ textField: UITextField) {
        delegate?.headerViewStopEditing(searchString: searchTextField.text!, dateString: dateTextField?.text)
    }
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        if (textField == dateTextField) {
            showDatePicker(textField)
        }
        return true
    }
}
